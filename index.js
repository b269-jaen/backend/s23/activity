// [SECTION] Part 1


let trainer = {
	name: 'Ash Ketchum',
	age: 10,
	friends: {haenn: ["May", "Max"], kanto: ["Brock", "Misty"]},
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	talk: function(){
		console.log("Pikachu! I choose you!");
	}
}
console.log(trainer);
console.log("Result of dot notation");
console.log(trainer.name);
console.log("Resule of square bracket notation:")
console.log(trainer['pokemon']);
console.log("Result of talk method")
trainer.talk();


// [SECTION] PART 2
function Pokemon(name, level) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    //Methods
    this.tackle = function(target) {
    	target.health = (target.health - this.attack);
        console.log(this.name + ' tackled ' + target.name);
        console.log(target.name + "'s health is now reduced to " + target.health);

        if(target.health <= 0){
        	target.faint();
        }    
    };
    this.faint = function(){
        console.log(this.name + ' fainted.');
    }

}


let Pika = new Pokemon("Pikachu", 16);
let Geo =  new Pokemon("Geodude", 12);
let Mew = new Pokemon("Mewtwo", 14);

console.log(Pika);
console.log(Geo);
console.log(Mew);

Pika.tackle(Geo);
Pika.tackle(Geo);

console.log(Geo);